import db from '../utils/db/index'

async function addStudent(entity) {
  const tmpJson = JSON.parse(entity)
  console.log('新增用户', tmpJson)
  try {
    await db.student.create({
      data: tmpJson
    })
  } catch (err) {
    console.log(err)
  }
}

async function delStudents(param) {
  const tmpJson = JSON.parse(param)
  const val = tmpJson.map((x) => x.id)
  console.log('收到', val)
  return await db.student.deleteMany({
    where: {
      id: {
        in: val
      }
    }
  })
}

async function uploadImg(entitys) {
  try {
    await entitys.forEach((entity) => {
      db.img.create({ data: entity }).then((res) => {
        console.log('保存图片', res)
      })
    })
  } catch (err: any) {
    console.log('保存图片信息错误', err)
  }
}

async function getAllImgs() {
  return await db.img.findMany()
}

async function getAllStudents() {
  return await db.student.findMany()
}

async function getFilterStudents(param) {
  const tmpJson = JSON.parse(param)
  return await db.student.findMany({
    where: {
      name: tmpJson.name,
      company: tmpJson.company
    }
  })
}

async function delImgs(param) {
  const tmpJson = JSON.parse(param)
  const val = tmpJson.map((x) => x.id)
  return await db.img.deleteMany({
    where: {
      id: {
        in: val
      }
    }
  })
}

async function changeState(param) {
  await db.img.updateMany({
    data: {
      state: 0
    }
  })
  return await db.img.updateMany({
    where: {
      id: param
    },
    data: {
      state: 1
    }
  })
}

async function getActive() {
  return await db.img.findMany({
    where: {
      state: 1
    }
  })
}

export {
  uploadImg,
  addStudent,
  getAllImgs,
  delImgs,
  changeState,
  getActive,
  getAllStudents,
  getFilterStudents,
  delStudents
}

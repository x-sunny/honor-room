import { app, shell, BrowserWindow, globalShortcut, ipcMain, dialog } from 'electron'
import { join } from 'path'
import fs from 'fs'
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import icon from '../../resources/icon.png?asset'
import {
  uploadImg,
  getAllImgs,
  delImgs,
  changeState,
  getActive,
  getAllStudents,
  getFilterStudents,
  addStudent,
  delStudents
} from './handles/index'
import { saveFile } from './utils/file/index'
import * as XLSX from 'js-xlsx'
import date from 'date-and-time'

let mainWindow

function createWindow(): void {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    show: false,
    //fullscreen: true,
    autoHideMenuBar: true,
    ...(process.platform === 'linux' ? { icon } : {}),
    webPreferences: {
      preload: join(__dirname, '../preload/index.js'),
      sandbox: false,
      webSecurity: false
    }
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.maximize()
    mainWindow.show()
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(join(__dirname, '../renderer/index.html'))
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.electron')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })

  ipcMain.handle('app:fullscreen', (event, arg) => {
    console.log(mainWindow.isFullScreen())
    if (mainWindow.isFullScreen()) {
      mainWindow.setFullScreen(false)
    } else {
      mainWindow.setFullScreen(true)
    }
  })

  ipcMain.handle('app:close', (event, arg) => {
    mainWindow = null
    app.exit()
  })

  ipcMain.handle('student:addStudent', (event, arg) => {
    event.preventDefault()
    return addStudent(arg)
  })

  ipcMain.handle('student:templateDownload', async (event, arg) => {
    event.preventDefault()
    let rel = false
    await dialog
      .showSaveDialog({
        title: '保存人员数据导入模板文件',
        defaultPath: '人员数据导入模板文件.xlsx',
        properties: ['createDirectory'],
        filters: [{ name: 'Xlsx', extensions: ['xlsx'] }]
      })
      .then((result) => {
        if (!result.canceled) {
          console.log(
            '准备赋值文件',
            result.filePath,
            `${app.getAppPath}/resources/templates/人员导入数据模板.xlsx`
          )

          fs.copyFile(
            `${app.getAppPath()}/resources/templates/人员导入数据模板.xlsx`,
            result.filePath,
            () => {}
          )
          rel = true
        }
      })
      .catch((err) => {
        console.log(err)
      })

    return new Promise((resolve) => {
      resolve(rel)
    })
    // return mainWindow.webContents.downloadURL(
    //   `${app.getAppPath}/resources/templates/人员导入数据模板.xlsx`
    // )
  })

  ipcMain.handle('student:uploadAvatar', async (event) => {
    event.preventDefault()
    let rel
    await dialog
      .showOpenDialog({
        properties: ['openFile'],
        filters: [{ name: 'Images', extensions: ['jpg', 'png', 'gif'] }]
      })
      .then((result) => {
        if (!result.canceled) {
          rel = saveFile(result.filePaths)
        }
      })
      .catch((err) => {
        console.log(err)
      })

    return new Promise((resolve) => {
      resolve(rel)
    })
  })

  ipcMain.handle('student:importData', async (event) => {
    event.preventDefault()
    let rel
    await dialog
      .showOpenDialog({
        properties: ['openFile'],
        filters: [{ name: '', extensions: ['xlsx'] }]
      })
      .then((result) => {
        if (!result.canceled) {
          console.log('sssss')
          const wb = XLSX.readFile(result.filePaths[0])
          const sheets = wb.Sheets['人员信息']
          const jsonData = XLSX.utils.sheet_to_json(sheets)
          if (jsonData != null && jsonData.length > 0) {
            jsonData.forEach((item) => {
              const today = date.parse(item['入伍年月'], 'YYYY/MM/DD')
              const url = item['照片']
              addStudent(
                JSON.stringify({
                  url: item['照片'],
                  name: item['姓名'],
                  sex: item['性别'] == '男' ? '1' : '0',
                  company: item['单位'],
                  monitor: item['新训班长'] ? item['新训班长'] : '',
                  joinDate: today
                })
              )
              console.log(
                {
                  url: item['照片'],
                  name: item['单位'],
                  sex: item['性别'],
                  company: item['单位'],
                  monitor: item['新训班长'],
                  joinDate: today
                },
                item['入伍年月']
              )
            })

            rel = '导入完成'
          }
        }
      })
      .catch((err) => {
        console.log(err)
      })

    return new Promise((resolve) => {
      resolve(rel)
    })
  })

  ipcMain.handle('student:delStudent', (event, arg) => {
    event.preventDefault()
    return delStudents(arg)
  })

  ipcMain.handle('img:getAll', () => {
    return new Promise((resolve, rejects) => {
      getAllImgs()
        .then((res) => {
          resolve(res)
        })
        .catch((err) => rejects(err))
    })
  })

  ipcMain.handle('student:getAll', () => {
    // return new Promise((resolve, rejects) => {
    //   getAllStudents()
    //     .then((res) => {
    //       resolve(res)
    //     })
    //     .catch((err) => rejects(err))
    // })

    return getAllStudents()
  })

  ipcMain.handle('student:getFilter', (arg) => {
    return new Promise((resolve, rejects) => {
      getFilterStudents(arg)
        .then((res) => {
          resolve(res)
        })
        .catch((err) => rejects(err))
    })
  })

  ipcMain.handle('web:flush', () => {
    mainWindow.webContents.reloadIgnoringCache()
  })

  ipcMain.handle('img:delByIds', (event, arg) => {
    event.preventDefault()
    return delImgs(arg)
  })

  ipcMain.handle('img:changeState', (event, arg) => {
    event.preventDefault()
    return changeState(arg)
  })

  ipcMain.handle('img:active', () => {
    return new Promise((resolve, rejects) => {
      getActive()
        .then((res) => {
          resolve(res)
        })
        .catch((err) => rejects(err))
    })
  })

  ipcMain.handle('dialg:openUpload', async () =>
    dialog
      .showOpenDialog({
        properties: ['openFile'],
        filters: [{ name: 'Images', extensions: ['jpg', 'png', 'gif'] }]
      })
      .then((result) => {
        if (!result.canceled) {
          const rel = saveFile(result.filePaths)
          const entitys = rel.map((item) => {
            return { url: item }
          })
          uploadImg(entitys)
        }
      })
      .catch((err) => {
        console.log(err)
      })
  )
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.

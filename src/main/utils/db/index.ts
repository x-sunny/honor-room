import { PrismaClient } from '@prisma/client'
import { join } from 'path'

const db = new PrismaClient({
  datasources: { db: { url: `file:${join(process.cwd(), 'resources/honor-room.sqlite3')}` } }
})
db.$connect().catch((err) => console.log(err))

export default db

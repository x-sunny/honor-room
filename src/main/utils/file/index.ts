import fs from 'fs'
import path from 'path'
import { v4 as uuidv4 } from 'uuid'

const homeDir = path.join(process.cwd(), 'resources', 'uploads')

const initDir = () => {
  if (!fs.existsSync(homeDir)) {
    try {
      fs.mkdirSync(homeDir)
      console.log('初始化目录成功')
    } catch (error: any) {
      console.log(error)
    }
  }
}

const saveFile = (imgs: string[]): Array<String> => {
  initDir()

  const rel: String[] = new Array()
  imgs.forEach((img) => {
    try {
      const extName = path.extname(img)
      const tmpFilePath = `${homeDir}\\${uuidv4()}${extName}`
      const reader = fs.createReadStream(img)
      const stream = fs.createWriteStream(tmpFilePath)
      reader.pipe(stream)

      rel.push(tmpFilePath)
    } catch (err: any) {
      console.log('保存文件错误', err)
    }
  })

  return rel
}

export { initDir, saveFile }

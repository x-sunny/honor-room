import { contextBridge, ipcRenderer } from 'electron'
import { electronAPI } from '@electron-toolkit/preload'

// Custom APIs for renderer
const api = {
  onAppClose: () => ipcRenderer.invoke('app:close'),
  onUploadAvatar: () => ipcRenderer.invoke('student:uploadAvatar'),
  onAddStudent: (param) => ipcRenderer.invoke('student:addStudent', param),
  onDelStudent: (param: string) => ipcRenderer.invoke('student:delStudent', param),
  onTemplateDownload: () => ipcRenderer.invoke('student:templateDownload'),
  onImportData: (param) => ipcRenderer.invoke('student:importData', param),
  onFullScreen: () => ipcRenderer.invoke('app:fullscreen'),
  /**
   * 打开保存文件的对话框
   * @returns
   */
  onWebFlush: () => ipcRenderer.invoke('web:flush'),
  onDialogUpload: () => ipcRenderer.invoke('dialg:openUpload'),
  onGetImgs: () => ipcRenderer.invoke('img:getAll'),
  onDelImgs: (param: string) => ipcRenderer.invoke('img:delByIds', param),
  onChangeState: (id) => ipcRenderer.invoke('img:changeState', id),
  onGetActive: () => ipcRenderer.invoke('img:active'),
  onGetStudents: () => ipcRenderer.invoke('student:getAll'),
  onGetFilterStudents: (param) => ipcRenderer.invoke('student:getFilter', param)
}

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld('electron', electronAPI)
    contextBridge.exposeInMainWorld('api', api)
  } catch (error) {
    console.error(error)
  }
} else {
  // @ts-ignore (define in dts)
  window.electron = electronAPI
  // @ts-ignore (define in dts)
  window.api = api
}

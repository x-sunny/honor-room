import { defineStore } from 'pinia'

interface IDialogState {
  openDialog: boolean
}

interface IFormSearch {
  name: string
  company: string
}

interface IStudent {
  id: string
  name: string
  company: string
  sex: number
  url: string
  joinDate: string
}

export const useStore = defineStore('openDialogState', {
  state: (): IDialogState => {
    return {
      openDialog: false
    }
  },
  actions: {
    setOpenDialogState(state: boolean) {
      this.openDialog = state
    }
  }
})

export const searchStore = defineStore('search', {
  state: (): IFormSearch => {
    return {
      formSearch: { name: '', company: '' }
    }
  },
  actions: {
    setFormSearch(formSearch: IFormSearch) {
      this.formSearch = formSearch
    }
  }
})

export const studentStore = defineStore('studentState', {
  state: (): IStudent => {
    return {
      student: {}
    }
  },
  actions: {
    setStudentState(state: IStudent) {
      this.student = state
    }
  }
})

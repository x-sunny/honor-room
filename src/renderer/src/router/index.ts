import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Layout from '@renderer/layout/index.vue'

export const routes: RouteRecordRaw[] = [
  {
    path: '/',
    meta: {
      logo: 'TakeawayBox',
      name: '能力对比'
    },
    redirect: '/home/index',
    component: Layout,
    children: [
      {
        path: '/home/index',
        name: '概览',
        component: () => import('@renderer/views/home/index.vue'),
        meta: {
          title: '概览'
        }
      }
    ]
  },
  {
    path: '/admin',
    meta: {
      name: 'admin'
    },
    redirect: '/admin/index',
    component: Layout,
    children: [
      {
        path: '/admin/index',
        name: 'adminIndex',
        component: () => import('@renderer/views/admin/index.vue'),
        meta: {
          title: '数据管理'
        }
      }
    ]
  },
  {
    path: '/search',
    name: 'adminSearch',
    component: Layout,
    redirect: '/search/index',
    children: [
      {
        path: '/search/index',
        name: 'searchIndex',
        component: () => import('@renderer/views/search/index.vue'),
        meta: {
          title: '人员信息检索'
        }
      }
    ]
  },
  {
    path: '/list',
    name: 'list',
    redirect: '/list/index',
    component: Layout,
    children: [
      {
        path: '/list/index',
        name: 'listIndex',
        component: () => import('@renderer/views/list/index.vue'),
        meta: {
          title: '历届学员学兵信息检索'
        }
      }
    ]
  },
  {
    path: '/detail',
    name: 'detail',
    redirect: '/detail/index',
    component: Layout,
    children: [
      {
        path: '/detail/index',
        name: 'detailIndex',
        component: () => import('@renderer/views/detail/index.vue'),
        meta: {
          title: '详情'
        }
      }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})
// router.beforeEach((to, from, next) => {
//   //beforeEach是router的钩子函数，在进入路由前执行
//   if (to.meta.title) {
//     //判断是否有标题
//     console.log(to.meta.title)
//     document.title = to.meta.title as string
//   } else {
//     document.title = '能力对比分析'
//   }
//   next()
// })
export default router
